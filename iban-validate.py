#!/usr/bin/env python

import sys
from schwifty import IBAN, BIC


if __name__ == "__main__":

    entered_iban = input("enter IBAN: ")
    try:
        iban = IBAN(entered_iban)
    except ValueError as err:
        print("IBAN INCORRECT")
        print("##############")
        print(err)
        sys.exit()

    print("IBAN OKAY!")

    print(f"\nIBAN:\n{iban.compact}\n")

    if iban.country_code == "DE":
        bic = BIC(str(iban.bic))
        print(f"BIC:\n{iban.bic}\n\nBank:\n{bic.bank_names[0]}\n")
